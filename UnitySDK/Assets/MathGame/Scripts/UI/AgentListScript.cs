﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AgentListScript : MonoBehaviour
{
    private List<GameObject> _goList;
    private bool             _toggle;

    private Text _agentCountText;
    private int _agentCount;

    // Start is called before the first frame update
    void Start()
    {
        _goList = new List<GameObject>();

        var legende = gameObject.transform.Find("Legende").gameObject;
        _goList.Add(legende);
        legende.SetActive(false);
        _agentCountText = gameObject.transform.Find("AgentCount").GetComponent<Text>();
        _agentCount = 0;
        _toggle = false;

        foreach (var unit in FindObjectsOfType<UnitAgent>())
        {
            AddEntry(unit);
        }
    }

    // Update is called once per frame
    void Update()
    {
       
    }

    public void toggleActive()
    {
        _toggle = !_toggle;
        
        foreach (var go in _goList)
        {
            go.SetActive(_toggle);
        }
    }

    private void AddEntry(UnitAgent unit)
    {
        var entry = Instantiate(Resources.Load<GameObject>("Prefabs/UI/AgentEntry"),transform);
        _goList.Add(entry);
        var trans = (RectTransform) entry.transform;
        trans.anchoredPosition = new Vector2(-12,-28*_agentCount);
        entry.GetComponent<AgentListEntryScript>().SetAgent(unit);
        entry.GetComponentInChildren<Text>().text = "Agent# " + _agentCount;
        entry.SetActive(_toggle);
        _agentCount++;
        _agentCountText.text = "Agents #: " + _agentCount.ToString();
    }
}
