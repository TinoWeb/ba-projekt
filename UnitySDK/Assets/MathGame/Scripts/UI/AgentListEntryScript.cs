﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AgentListEntryScript : MonoBehaviour
{
    public List<GameObject> _goList;
    private bool _toggle = true;

    private UnitAgent _agent;

    public Text scoreText;
    public Text cumScoreText;
    public Text episodeText;
    public Text winLoseText;
    public Text stepText;

    // Start is called before the first frame update
    void Start()
    {
    }

    public void SetAgent(UnitAgent agent)
    {
        _agent = agent;
    }
    
    // Update is called once per frame
    void Update()
    {
        scoreText.text = _agent.GetReward().ToString("N4");
        cumScoreText.text = _agent.GetCumulativeReward().ToString("N4");
        var episodeCount = _agent.GetEpisodeCount();
        episodeText.text = episodeCount.ToString();
        var winCount = _agent.GetWinCount();
        winLoseText.text = winCount.ToString() + " / " + (episodeCount - winCount).ToString(); 
        stepText.text = _agent.GetStepCount().ToString();
    }

    public void toggleActive()
    {
        _toggle = !_toggle;
        
        foreach (var go in _goList)
        {
            go.SetActive(_toggle);
        }
    }
    
    
}
