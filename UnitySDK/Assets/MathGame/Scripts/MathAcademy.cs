﻿using System;
using System.Collections;
using System.Collections.Generic;
using MLAgents;
using UnityEngine;

public class MathAcademy : Academy
{
    
    private List<GameObject> _gamesList = new List<GameObject>();

    // Start is called before the first frame update
    void Start()
    {
        // Get all controllers in the scene and add them to the controller list
        foreach (var game in GameObject.FindGameObjectsWithTag("Game"))
        {
            _gamesList.Add(game);
        }
    }
    
    public override void AcademyReset()
    {
        // Check if we are training
        if (!GetIsInference())
        {
            // Set GameParameter if we are training for each game instance
            foreach (var game in _gamesList)
            {    
                // get map gameobject
                var mapGO = game.transform.GetChild(0).gameObject;
                if (mapGO.name.Equals("Map"))
                {
                    // set scale
                    var transformLocalScale = mapGO.transform.localScale;
                    transformLocalScale.x = (float) resetParameters["MapSize"];
                    transformLocalScale.z = (float) resetParameters["MapSize"];
                    mapGO.transform.localScale = transformLocalScale;
                }
            }
        }
    }

    public override void AcademyStep()
    {
        base.AcademyStep();
    }
}
