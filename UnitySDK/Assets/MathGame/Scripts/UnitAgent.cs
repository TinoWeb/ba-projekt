﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using MLAgents;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class UnitAgent : Agent
{
    public  GameObject map;
    private float      _resetRange;

    public  GameObject movementTarget;
    private Vector3    _bestVectorToTarget;
    private float      _distanceAtStart;


    private float     _diaMovementCorrection=1f;
    public  float     unitMovementSpeed;
    public  float     sidewardsMovementPenalty;
    public  float     unitRotationSpeed;
    public  Rigidbody agentRB;

    private int _episodeCount = 0;
    private int _winCount     = 0;


    public override void InitializeAgent()
    {
        _resetRange = 45f * map.transform.localScale.x;
        ResetMovementTarget();
    }

    public override void CollectObservations()
    {
        // Position (normalized)
        AddVectorObs(gameObject.transform.localPosition / (25f * map.transform.localScale.x));
        // Positon (std)
        //AddVectorObs(gameObject.transform.localPosition);
        
        // Rotation (normalized)
        AddVectorObs(gameObject.transform.rotation.eulerAngles.y / 360f);
        // Rotation (std)
        //AddVectorObs(gameObject.transform.rotation.eulerAngles.y);
        
        // Target position (normalized)
        AddVectorObs(movementTarget.transform.localPosition / (25f * map.transform.localScale.x));
        // Target position (std)
        //AddVectorObs(movementTarget.gameObject.transform.position);
    }

    public override void AgentAction(float[] vectorAction, string textAction)
    {
        if (brain.brainParameters.vectorActionSpaceType == SpaceType.Discrete)
        {
            // Permanently negative Reward to accelerate training and agents behavior
            AddReward(-0.0005f);

            //
            // Move Unit
            //
            var dirToGo               = Vector3.zero;
            var rotateDir             = Vector3.zero;
            var dirToGoForwardAction  = (int) vectorAction[0];
            var rotateDirAction       = (int) vectorAction[1];
            var dirToGoSidewardAction = (int) vectorAction[2];

            // Calc Direction and Rotation
            if (dirToGoForwardAction == 1)
                // Move forwards
                dirToGo = unitMovementSpeed * transform.forward;
            else if (dirToGoForwardAction == 2)
                // Move backwards
                dirToGo = -0.75f * unitMovementSpeed * transform.forward;

            if (dirToGoSidewardAction == 1)
                // Move sidewards (left)
                dirToGo += (-sidewardsMovementPenalty * unitMovementSpeed * transform.right);
            else if (dirToGoSidewardAction == 2)
                // Move sidewards (right)
                dirToGo += sidewardsMovementPenalty * unitMovementSpeed * transform.right;

            
            // Correct speed(magnitude) of simultaneous forwards and sidewards movement
            if (dirToGoForwardAction != 0 && dirToGoSidewardAction != 0)
            {
                dirToGo /= _diaMovementCorrection;
                //Debug.Log(dirToGo.magnitude.ToString());
            }

            if (rotateDirAction == 1)
                // Rotate left
                rotateDir = Vector3.up * -1f;
            else if (rotateDirAction == 2)
                // Rotate right 
                rotateDir = Vector3.up * 1f;

            // Perform Movement
            transform.Rotate(rotateDir, Time.fixedDeltaTime * unitRotationSpeed);

            agentRB.AddForce(dirToGo, ForceMode.VelocityChange);

            
            //
            // Reward for nearing the target (positive and negative possible)
            //
            // Check distance
            var distanceVector = transform.localPosition - movementTarget.transform.localPosition;
            var distanceChange = _bestVectorToTarget.magnitude - distanceVector.magnitude;
            AddReward(distanceChange / _distanceAtStart);
            _bestVectorToTarget = distanceVector;
        }
    }

    void OnTriggerStay(Collider col)
    {
        if (col.gameObject.CompareTag("movementTarget"))
        {
            AddReward(40f);
            _winCount++;
            _episodeCount++;
            Done();
        }
        
        if (col.gameObject.CompareTag("wall"))
        {
            AddReward(-1f);
            _episodeCount++;
            Done();
        }
    }

    public override void AgentReset()
    {
        _resetRange = 45f * map.transform.localScale.x;
        ResetMovementTarget();

        // Reset position to the middle of the map
        transform.position = map.transform.position;
        // Reset forces on the Agent
        agentRB.velocity = Vector3.zero;
        agentRB.angularVelocity = Vector3.zero;
        
    }

    void ResetMovementTarget()
    {
        if (movementTarget == null)
        {
            movementTarget = GameObject.FindWithTag("movementTarget");
        }

        var position = movementTarget.transform.localPosition;
        var randomPos = new Vector3(Random.Range(-_resetRange, _resetRange), position.y,
            Random.Range(-_resetRange, _resetRange));

        movementTarget.gameObject.SetActive(false);
        position                               = randomPos;
        movementTarget.transform.localPosition = position;
        movementTarget.gameObject.SetActive(true);

        _bestVectorToTarget = transform.localPosition - position;

        _distanceAtStart = _bestVectorToTarget.magnitude;
    }

    private void Start()
    {
        _diaMovementCorrection = Mathf.Sqrt(unitMovementSpeed + unitMovementSpeed * sidewardsMovementPenalty);
    }

    public void Update()
    {
        if (GetCumulativeReward() < -10f || GetStepCount() > 3000)
        {
            AddReward(-20f);
            _episodeCount++;
            Done();
        }
    }

    public void LateUpdate()
    {
        var transform1 = transform;
        transform1.eulerAngles = new Vector3(0, transform1.eulerAngles.y, 0);
    }


    public int GetEpisodeCount()
    {
        return _episodeCount;
    }

    public int GetWinCount()
    {
        return _winCount;
    }
}