﻿using System;
using MLAgents;
using UnityEngine;
using UnityEngine.Experimental.PlayerLoop;

public class AgentScript : Agent
{
    [HideInInspector] public int  woodPileCount;
    [HideInInspector] public bool won = false;

    public float     unitMovementSpeed;
    public float     unitRotationSpeed;
    public Rigidbody agentRB;
    public int       maxSteps;

    
    private float    _diaMovementCorrection =1f;
    private          Vector3    _startPosition;
    private          bool       _canPickUp = true;
    private          GameObject _targetObj;
    private          Vector3    _lastVectorToTarget;
    private          float      _distanceAtStart;

    public event System.Action                          AgentIsDone;
    public event System.Action<AgentScript, GameObject> ItemPickedUp;

    // Variables for statistics
    [HideInInspector] public int episodeCount = 0;
    [HideInInspector] public int stepCount    = 0;
    [HideInInspector] public int winCount     = 0;
    [HideInInspector] public int pickUpCount  = 0;
    [HideInInspector] public int putDownCount = 0;

    public override void InitializeAgent()
    {
        _startPosition = transform.localPosition;
        _diaMovementCorrection = Mathf.Sqrt(unitMovementSpeed + unitMovementSpeed * 0.9f);
    }

    public override void CollectObservations()
    {
    }

    public override void AgentAction(float[] vectorAction, string textAction)
    {
        // Permanently negative Reward to accelerate training and agents behavior
        AddReward(-0.0005f);
        
        stepCount++;

        if (brain.brainParameters.vectorActionSpaceType == SpaceType.Discrete)
        {
            //
            // Move Unit
            //
            var dirToGo               = Vector3.zero;
            var rotateDir             = Vector3.zero;
            var dirToGoForwardAction  = (int) vectorAction[0];
            var rotateDirAction       = (int) vectorAction[1];
            var dirToGoSidewardAction = (int) vectorAction[2];

            // Calc Direction and Rotation
            if (dirToGoForwardAction == 1)
                // Move forwards
                dirToGo = unitMovementSpeed * transform.forward;
            else if (dirToGoForwardAction == 2)
                // Move backwards
                dirToGo = -0.75f * unitMovementSpeed * transform.forward;

            if (dirToGoSidewardAction == 1)
                // Move sidewards (left)
                dirToGo += (-0.9f * unitMovementSpeed * transform.right);
            else if (dirToGoSidewardAction == 2)
                // Move sidewards (right)
                dirToGo += 0.9f * unitMovementSpeed * transform.right;

            // Correct speed(magnitude) of simultaneous forwards and sidewards movement
            if (dirToGoForwardAction != 0 && dirToGoSidewardAction != 0)
            {
                dirToGo /= _diaMovementCorrection;
                //Debug.Log(dirToGo.magnitude.ToString());
            }

            // Rotate
            if (rotateDirAction == 1)
                // Rotate left
                rotateDir = Vector3.up * -1f;
            else if (rotateDirAction == 2)
                // Rotate right 
                rotateDir = Vector3.up * 1f;

            // Slow down if no movment action chosen
            if (dirToGoForwardAction == 0 && dirToGoSidewardAction == 0)
            {
                dirToGo = -0.2f * agentRB.velocity;
            }
            
            // Perform Movement
            transform.Rotate(rotateDir, Time.fixedDeltaTime * unitRotationSpeed);

            agentRB.AddForce(dirToGo, ForceMode.VelocityChange);

            if (_targetObj != null)
            {
                // Check distance
                var distanceVector = transform.position - _targetObj.transform.position;
                var distanceChange = _lastVectorToTarget.magnitude - distanceVector.magnitude;
                AddReward(distanceChange / _distanceAtStart);
                _lastVectorToTarget = distanceVector;
            }
        }
    }


    public override void AgentReset()
    {
        stepCount = 0;

        transform.localPosition = _startPosition;
        agentRB.angularVelocity = Vector3.zero;
        agentRB.velocity        = Vector3.zero;
        _canPickUp              = true;

        pickUpCount  = 0;
        putDownCount = 0;
    }

    public void SetNewTarget(GameObject newTarget)
    {
        _targetObj = newTarget;

        if (newTarget == null) return;

        _lastVectorToTarget = transform.position - _targetObj.transform.position;
        _distanceAtStart    = _lastVectorToTarget.magnitude;
    }

    public override void AgentOnDone()
    {
        //Debug.Log("Agent Done!");
        episodeCount++;
        AgentIsDone();
    }

    void OnTriggerStay(Collider col)
    {
        if (col.gameObject.CompareTag("WoodPile"))
        {
            //Debug.Log("Collision with WoodPile");
            PickUp(col.gameObject);
            Done();
            
        }

        if (col.gameObject.CompareTag("WoodStore"))
        {
            //Debug.Log("Collision with WoodStore");
            PutDown();
        }

        if (col.gameObject.CompareTag("Spider"))
        {
            //Debug.Log("Collision with Spider");
            col.gameObject.SetActive(false);
            won = false;
            Done();
        }
        
        if (col.gameObject.CompareTag("wall"))
        {
            //Debug.Log("Collision with wall");
            AddReward(-0.1f);
        }
    }

    private void PutDown()
    {
        if (!_canPickUp)
        {
            putDownCount++;
            AddReward(8);
            _canPickUp = !_canPickUp;
            _targetObj = null;

            if (putDownCount == woodPileCount)
            {
                won = true;
                winCount++;
                Done();
            }
        }
        else
        {
            Debug.Log("Nothing to put down!");
        }
    }

    private void PickUp(GameObject obj)
    {
        if (_canPickUp)
        {
            _canPickUp = !_canPickUp;
            pickUpCount++;
            ItemPickedUp(this, obj);
            AddReward(20);
        }
        else
        {
            Debug.Log("Cant pick up!");
        }
    }

    public void Update()
    {
        var transf = transform;
        transf.eulerAngles = new Vector3(0, transf.eulerAngles.y, 0);

        if (maxSteps != 0 && stepCount > maxSteps)
        {
            AddReward(-5);
            Done();
        }

        if (GetCumulativeReward() <=-20)
        {
            Done();
        }
    }

    public void FixedUpdate()
    {
        //Debug.Log(agentRB.velocity.magnitude.ToString());
    }
}