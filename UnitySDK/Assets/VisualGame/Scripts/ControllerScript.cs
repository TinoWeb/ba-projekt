﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public enum ControllerMode
{
    Unknown      = -1,
    GameMode     = 0,
    TrainingMode = 1,
    DemoMode     = 2
}

public class ControllerScript : MonoBehaviour
{
    public ControllerMode controllerMode;
    public int          mapSize;
    public int          maxPileCount;
    public int          maxSpiderCount;
    public int          woodPileCount;
    public int          spiderCount;
    public float        woodPileColliderRadius;
    public float        woodStoreColliderRadius;
    public int          loopCount = 1;

    private int               loopCounter = 0;
    public  GameObject        forest;
    private GameObject        _woodStore;
    private List<GameObject>  _woodPileList = new List<GameObject>();
    private List<GameObject>  _spiderList   = new List<GameObject>();
    private List<AgentScript> _agentList    = new List<AgentScript>();


    // Start is called before the first frame update
    void Start()
    {
        foreach (var agent in GetComponentsInChildren<AgentScript>())
        {
            _agentList.Add(agent);
            agent.AgentIsDone  += OnAgentDone;
            agent.ItemPickedUp += OnItemPickedUp;
        }

        _woodStore = GameObject.FindGameObjectWithTag("WoodStore");
    }

    // Update is called once per frame
    void Update()
    {
    }

    private void OnItemPickedUp(AgentScript agent, GameObject obj)
    {
        // assuming its a wood pile....
        
        // set new target
        agent.SetNewTarget(_woodStore);
        
        // remove GameObject
        RemoveWoodPile(obj);
    }

    private void OnAgentDone()
    {
        // Check if all Agents of this Instance are done
        var allDone  = true;
        var agentWon = false;
        foreach (var agent in _agentList)
        {
            if (!agent.isActiveAndEnabled) allDone = false;
            if (agent.won) agentWon                = true;
        }

        if (allDone) EpisodeFinished(agentWon);
    }

    public void EpisodeFinished(bool won)
    {
        // Modify GameParameters if in GameMode or DemoMode
        // If in TrainingMode this parameters are set by Academy
        if (controllerMode == ControllerMode.GameMode)
        {
            // If Game won increase complexity, if not repeat with the same parameters
            if (won)
            {
                // increase number of piles to gather
                if (woodPileCount < maxPileCount) woodPileCount++;

                // activate forrest on Level 3
                if (woodPileCount >= 3)
                {
                    forest.SetActive(true);
                }

                // activate enemies on Level 3
                if (woodPileCount >= 3)
                {
                    if (spiderCount < maxSpiderCount) spiderCount++;
                }
            }
        }
        else if (controllerMode == ControllerMode.DemoMode)
        {
            loopCounter++;
            // If enough loops played, increase complexity. If not repeat with the same parameters
            if (loopCounter >= loopCount)
            {
                // increase number of piles to gather
                if (woodPileCount < maxPileCount) woodPileCount++;

                // activate forrest on Level 3
                if (woodPileCount >= 3)
                {
                    forest.SetActive(true);
                }

                // activate enemies on Level 3
                if (woodPileCount >= 3)
                {
                    if (spiderCount < maxSpiderCount) spiderCount++;
                }

                // Reset counter
                loopCounter = 0;
            }
        }

        ResetGame();
    }

    public void ResetGame()
    {
        // Remove WoodPiles and Spiders
        RemoveWoodPiles();
        RemoveSpiders();

        // Set WoodStoreColliderRadius if its not default 
        if (woodStoreColliderRadius > 0) _woodStore.GetComponent<CapsuleCollider>().radius = woodStoreColliderRadius;

        // Spawn new WoodPiles and Spiders
        SpawnWoodPiles(woodPileCount);
        SpawnSpiders(spiderCount);
        
        // Reset the Agents in the Game
        ResetAgents();
    }

    private void ResetAgents()
    {
        foreach (var agent in _agentList)
        {
            agent.enabled       = false;
            agent.woodPileCount = woodPileCount;
            agent.SetNewTarget(_woodPileList.Count == 1 ? _woodPileList.First() : null);

            agent.AgentReset();
            
            agent.enabled = true;
        }
    }

    private void SpawnSpiders(int count)
    {
        for (var i = 0; i < count; i++)
        {
            // Instantiate a instance of the spider prefab
            var spider = Instantiate(Resources.Load<GameObject>("Prefabs/Spider"), transform);
            // Place the spider
            SetRandomPosition(spider);
            // Ensure the GameObject is set active
            spider.SetActive(true);
            // Add the instance to the spider list
            _spiderList.Add(spider);
        }
    }

    private void SpawnWoodPiles(int count)
    {
        for (var i = 0; i < count; i++)
        {
            // Instantiate a instance of the wood pile prefab
            var pile = Instantiate(Resources.Load<GameObject>("Prefabs/WoodPile"), transform);
            // Place the pile
            SetRandomPosition(pile);
            // Set woodPileColliderRadius if its not default
            if (woodPileColliderRadius > 0) pile.GetComponent<SphereCollider>().radius = woodPileColliderRadius;
            // Ensure the GameObject is set active
            pile.SetActive(true);
            // Add the instance to the wood pile list
            _woodPileList.Add(pile);
        }
    }


    private void RemoveSpiders()
    {
        // Destroy all Spiders in the list
        foreach (var spider in _spiderList)
        {
            GameObject.Destroy(spider);
        }

        // Clear the list
        _spiderList.Clear();
    }

    private void RemoveWoodPiles()
    {
        // Destroy all wood piles int the list
        foreach (var pile in _woodPileList)
        {
            GameObject.Destroy(pile);
        }

        // Clear the list
        _woodPileList.Clear();
    }

    public void RemoveWoodPile(GameObject obj)
    {
        foreach (var pile in _woodPileList)
        {
            if (!pile.Equals(obj)) continue;

            _woodPileList.Remove(pile);
            Destroy(pile);
            break;
        }
    }

    private void SetRandomPosition(GameObject obj)
    {
        // Get random position
        var pos = new Vector3(Random.Range(-mapSize, mapSize), 0.2f, Random.Range(-mapSize, mapSize));
        
        // Block position of the hut (X: (-30) - (-20) Z: (48) -  (38))
        while ((pos.x > -30f && pos.x < -20f) && (pos.z > 38f && pos.z < 48f))
        {
            pos = new Vector3(Random.Range(-mapSize, mapSize), 0.2f, Random.Range(-mapSize, mapSize));
        }
        
        // set position 
        obj.transform.localPosition = pos;

        
    }
}