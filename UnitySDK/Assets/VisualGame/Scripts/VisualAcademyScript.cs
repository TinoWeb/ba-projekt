﻿using System.Collections;
using System.Collections.Generic;
using MLAgents;
using UnityEngine;

public class VisualAcademyScript : Academy
{
    private List<ControllerScript> _controllerList = new List<ControllerScript>();


    // Start is called before the first frame update
    void Start()
    {
        // Get all controllers in the scene and add them to the controller list
        foreach (var controller in FindObjectsOfType<ControllerScript>())
        {
            _controllerList.Add(controller);
        }
    }

    // Update is called once per frame
    void Update()
    {
    }

    public override void AcademyReset()
    {
        // Check if we are training
        if (!GetIsInference())
        {
            // Set GameParameter if we are training
            SetResetParamsAndReset();
        }
        else
        {
            // Reset each game if we are not training
            foreach (var controller in _controllerList)
            {
                controller.ResetGame();
            }
        }
    }

    private void SetResetParamsAndReset()
    {
        foreach (var controller in _controllerList)
        {
            controller.woodPileCount = (int) resetParameters["woodPileCount"];
            controller.spiderCount = (int) resetParameters["spiderCount"];
            controller.forest.SetActive(resetParameters["forestActive"] > 0);
            controller.woodPileColliderRadius = resetParameters["woodPileColliderRadius"];
            controller.woodStoreColliderRadius = resetParameters["woodStoreColliderRadius"];  
            
            controller.ResetGame();
        }
    }
}