﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UiControllerScript : MonoBehaviour
{
    private AgentScript _agent;

    public Text episodesText;
    public Text stepsText;
    public Text availablePilesText;
    public Text gatheredPilesText;
    public Text winText;
    public Text rewardText;
    public Text cumRewardText;

    private bool active = false;
    
    // Start is called before the first frame update
    void Start()
    {
        _agent = GameObject.FindWithTag("agent").GetComponent<AgentScript>();
        if (_agent != null) active = true;
    }

    // Update is called once per frame
    void Update()
    {
        if(!active) return;

        episodesText.text = (_agent.episodeCount+1).ToString();
        stepsText.text = _agent.stepCount.ToString();
        availablePilesText.text = _agent.woodPileCount.ToString();
        gatheredPilesText.text = _agent.pickUpCount.ToString() + " (" +_agent.putDownCount.ToString() + ")";
        winText.text = _agent.winCount.ToString() + "/" + (_agent.episodeCount -_agent.winCount).ToString();
        rewardText.text = _agent.GetReward().ToString("n7");
        cumRewardText.text = _agent.GetCumulativeReward().ToString("n6");
    }
}
